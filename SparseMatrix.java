import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;

public class question1a {

	static int row_size=0;
	static int column_size=0;
	
	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
			
		   	
		     //count number of rows and columns
			 Scanner sc0 = new Scanner(new BufferedReader(new FileReader("C:\\Users\\Irfaan Coonjbeeharry\\Desktop\\sparseMatrix.txt")));
				while(sc0.hasNextLine()) {
     		            String[] line = sc0.nextLine().trim().split(" ");
			            column_size=line.length;
			            row_size++;
			         }
			int SparseArray[][] = new int[row_size][column_size];
				
				
			Scanner sc = new Scanner(new BufferedReader(new FileReader("C:\\Users\\Irfaan Coonjbeeharry\\Desktop\\sparseMatrix.txt")));
			while(sc.hasNextLine()) {
		         for (int i=0; i<SparseArray.length; i++) {
		            String[] line = sc.nextLine().trim().split(" ");
		            column_size=line.length;
		            for (int j=0; j<line.length; j++) {
		            	SparseArray[i][j] = Integer.parseInt(line[j]);
		            }
		         }
		      }
			
		    // Print Array
					for (int i=0; i<row_size; i++) {
		            for (int j=0; j<column_size; j++) {
		              System.out.print(SparseArray[i][j]+ " ");
		            }
		            System.out.println();
		         }
		       System.out.println("#End of Printing Array#");
		   
	   
	//End of Read File to Array
		    // Start converting to SparseTriplet	    
		      int size = 0;
		      for (int row = 0; row < row_size; row++) {
		          for (int column = 0; column < column_size; column++) {
		              if (SparseArray[row][column] != 0) {
		                  size++;
		              }
		          }
		      }
   // End Finding total non-zero values in the sparse matrix	      
	
		 // Defining result Matrix
		 int resultMatrix[][] = new int[size][3];
		 
		 
		// Generating result matrix
		    int k = 0;
		    for (int row = 0; row < row_size; row++) {
		        for (int column = 0; column < column_size; column++) {
		            if (SparseArray[row][column] != 0)
		            {
		                resultMatrix[k][0] = row;
		                resultMatrix[k][1] = column;
		                resultMatrix[k][2] = SparseArray[row][column];
		                k++;
		            }
		        }
		    }
		    
		 // Displaying result matrix
		    System.out.println("Triplet Representation from matrix: "); 
		    for (int row=0; row<size; row++)
		    {
		        for (int column = 0; column<3; column++) {
		        	  System.out.print(resultMatrix[row][column]+" ");
		        }
		        System.out.println();
		    }
		   
			 // Writing result matrix into file sparseTriple
		    FileWriter myWriter = new FileWriter("C:\\Users\\Irfaan Coonjbeeharry\\Desktop\\sparseTriple.txt");
		    for (int row=0; row<size; row++)
		    {
		        for (int column = 0; column<3; column++) {
		        	myWriter.write(resultMatrix[row][column]+" ");
		        }
		        myWriter.write("\n");
		    }
		    myWriter.close();	      
		      
		 //  Read file matrix from file sparseTriple  
		    
		    int TripleMatrix[][] = new int[size][3];
	
		    Scanner sc1 = new Scanner(new BufferedReader(new FileReader("C:\\Users\\Irfaan Coonjbeeharry\\Desktop\\sparseTriple.txt")));
			while(sc1.hasNextLine()) {
		         for (int i=0; i<size; i++) {
		            String[] line = sc1.nextLine().trim().split(" ");
		            for (int j=0; j<3; j++) {
		            	TripleMatrix[i][j] = Integer.parseInt(line[j]);
		            }
		         }
		      }
			
			
				Node head_linkedlist = construct(TripleMatrix, 0, 0, size, 3); 
				 System.out.println("Display Triplet Matrix from linkedlist : "); 
		        display(head_linkedlist); 
		        OverwriteFileSM(head_linkedlist);
		        OverwriteFileST(head_linkedlist);
	       
		    
		    
		    
	
	
	}
	
		static class Node { 
	        int data; 
	        Node right; 
	        Node down; 
	    }; 


static Node construct(int arr[][], int i, int j,  int m, int n) { 
	    				// return if i or j is out of bounds 
	    			if (i > m - 1 || j > n - 1) {
	    						return null; 
	    			}

// create a new node for current i and j 
// and recursively allocate its down and 
// right pointers 
	    				
	    Node temp = new Node(); 
	    temp.data = arr[i][j]; 
	    temp.right = construct(arr, i, j + 1, m, n);  //j=column
	    temp.down = construct(arr, i + 1, j, m, n);  //i=row
	    return temp; 
	    } 

// utility function for displaying 
// linked list data 
	static void display(Node head) { 

// pointer to move right 
	Node Rp; 

// pointer to move down 
	Node Dp = head; 

// loop till node->down is not NULL 
	while (Dp != null) { 
		Rp = Dp; 

// loop till node->right is not NULL 
		while (Rp != null) { 
			System.out.print(Rp.data + " "); 
			Rp = Rp.right; 
			} 
		System.out.println(); 
		Dp = Dp.down; 
	} 
} 
	
	
	static void OverwriteFileST(Node head) throws Exception { 

		// pointer to move right 
			Node Rp; 

		// pointer to move down 
			Node Dp = head; 

		// loop till node->down is not NULL 
			 FileWriter myWriter = new FileWriter("C:\\Users\\Irfaan Coonjbeeharry\\Desktop\\sparseTriple1.txt");
			while (Dp != null) { 
				Rp = Dp; 

		// loop till node->right is not NULL 
			   
				while (Rp != null) { 
					myWriter.write(Rp.data + " ");
					Rp = Rp.right; 
					} 
				 myWriter.write("\n");
				Dp = Dp.down; 
			}
			  myWriter.close();	 
		} 
	
	static void OverwriteFileSM(Node head) throws Exception { 

		// pointer to move right 
			Node Rp; 

		// pointer to move down 
			Node Dp = head; 

		// loop till node->down is not NULL 
			
			 FileWriter myWriter = new FileWriter("C:\\Users\\Irfaan Coonjbeeharry\\Desktop\\sparseMatrix1.txt");
			 int SparseArray[][] = new int[row_size][column_size];
			while (Dp != null) { 
				Rp = Dp; 

		// loop till node->right is not NULL 
				while (Rp != null) { 
					SparseArray[Rp.data][Rp.right.data]=Rp.right.right.data;
					Rp = null; 
					} 
				Dp = Dp.down; 
			}
			
			//write to file
				for (int row=0; row<row_size; row++)
			    {
			        for (int column = 0; column<column_size; column++) {
			        	myWriter.write(SparseArray[row][column]+" ");
			        }
			        myWriter.write("\n");
			    }
			  myWriter.close();	 
		}
	
	
	
	


}

